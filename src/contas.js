import express from 'express';
import fs from 'fs';
import { promisify } from 'util';

const router = express.Router();
const writeFile = promisify(fs.writeFile);
const readFile = promisify(fs.readFile);

// INCLUSÃO DE NOVA CONTA
router.post('/', async (req, res) => {
  try {
    let conta = req.body;
    const data = JSON.parse(await readFile(global.fileName, 'utf8'));
    conta = { id: data.nextId++, ...conta, timestamp: new Date() };
    data.contas.push(conta)
    await writeFile(global.fileName, JSON.stringify(data));

    res.status(200).send('Conta incluída com sucesso! ');
    res.end();

    logger.info(`POST /conta - ${JSON.stringify(conta)}`);
  } catch (err) {
    res.status(400).send({ error: err.message });
  }
});

// RETORNA TODAS AS CONTAS
router.get('/', async (_, res) => {
  try {
    const data = JSON.parse(await readFile(global.fileName, 'utf8'));
    // deleta propriedade que nao deseja mostrar ao usuario
    delete data.nextId;

    res.send(data);

    logger.info("GET /conta");
  } catch (err) {
    res.status(400).send({ error: err.message });
  }
});

// RETORNA APENAS UMA CONTA CONFORME ID INFORMADO
router.get('/:id', async (req, res) => {
  try {
    const data = JSON.parse(await readFile(global.fileName, 'utf8'));
    const conta = data.contas.find(conta => conta.id === parseInt(req.params.id, 10));
    
    if (conta) {
      res.send(conta);
    } else {
      res.status(400).send('ERRO: Conta não encontrada');
      res.end();
    } 
    
    logger.info(`GET /conta - " ${req.params.id}`);
  } catch (err) {
    res.status(400).send({ error: err.message });
  }
});

// DELETA UMA CONTA CONFORME ID INFORMADO
router.delete('/:id', async (req, res) => {
  try {
    const data = JSON.parse(await readFile(global.fileName, 'utf8'));
    data.contas = data.contas.filter(conta => conta.id !== parseInt(req.params.id, 10));
    await writeFile(global.fileName, JSON.stringify(data));

    res.status(200).send('Conta excluída com sucesso! ');
    res.end();

    logger.info(`DELETE /conta - " ${req.params.id}`);
  } catch (err) {
    res.status(400).send({ error: err.message });
  }
});

// ALTERA O SALDO DA CONTA - DEPOSITO
router.put('/deposito', async (req, res) => {
  try {
    const newconta = req.body;
    const data = JSON.parse(await readFile(global.fileName, 'utf8'));
    let oldcontaIndex = data.contas.findIndex(conta => conta.id === newconta.id);

    data.contas[oldcontaIndex].balance += newconta.balance;
    data.contas[oldcontaIndex].timestamp = new Date();
    await writeFile(global.fileName, JSON.stringify(data));

    res.status(200).send('Depósito realizado com sucesso! ');
    res.end();

    logger.info(`PUT /conta/deposito - " ${JSON.stringify(data)}`);
  } catch (err) {
    res.status(400).send({ error: err.message });
  }
});

// ALTERA O SALDO DA CONTA - SAQUE
router.put('/saque', async (req, res) => {
    try {
      const newconta = req.body;
      const data = JSON.parse(await readFile(global.fileName, 'utf8'));
      let oldcontaIndex = data.contas.findIndex(conta => conta.id === newconta.id);
  
      if (data.contas[oldcontaIndex].balance >= newconta.balance) {
        data.contas[oldcontaIndex].balance -= newconta.balance;
        data.contas[oldcontaIndex].timestamp = new Date();
        await writeFile(global.fileName, JSON.stringify(data));
  
         res.status(200).send('Saque realizado com sucesso! ');
        res.end();
      } else {
        res.status(400).send('ERRO: Saldo insuficiente para o saque solicitado! ');
        res.end();
      } 
  
      logger.info(`PUT /conta/saque - " ${JSON.stringify(data)}`);
    } catch (err) {
      res.status(400).send({ error: err.message });
    }
  });

export default router;

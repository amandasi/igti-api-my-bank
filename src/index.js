import express from 'express';
import fs from 'fs';
import { promisify } from 'util';
import winston from 'winston';
import contasRouter from './contas.js';

const app = express();
const port = 3000;
const exists = promisify(fs.exists);
const writeFile = promisify(fs.writeFile);
const { combine, timestamp, label, printf } = winston.format;
const myFormat = printf(({ level, message, label, timestamp }) => {
  return `${timestamp} [${label}] ${level}: ${message}`;
});
global.fileName = 'contas.json';

app.use(express.json());
app.use(express.static('public'));
app.use('/images', express.static('public'));
app.use('/conta', contasRouter);

global.logger = winston.createLogger({
    level: 'silly',
    transports: [
      new (winston.transports.Console)(),
      new (winston.transports.File)({ filename: 'contas-control-api.log' })
    ],
    format: combine(
      label({ label: 'contas-control-api' }),
      timestamp(),
      myFormat
    )
  });
  
  app.listen(3000, async () => {
    try {
      const fileExists = await exists(global.fileName);
      if (!fileExists) {
        const initialJson = {
          nextId: 1,
          contas: [] 
        };
        await writeFile(global.fileName, JSON.stringify(initialJson));
      }
    } catch (err) {
      logger.error(err);
    }
    logger.info('API started!');
  });
  

/*
app.use((req, res, next) =>{
    console.log(new Date());
    next();
})

app.get('/',(req,res) => res.send('Hello World Bank Mandy!!!'));

app.listen(port,() =>{
    console.log(`App.listening on port ${port}`);
});
*/
  
